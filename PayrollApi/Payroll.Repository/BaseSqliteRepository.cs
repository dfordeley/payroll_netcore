﻿using NPoco;
using Payroll.Data.Data;
using Payroll.Data.Entity;
using Payroll.Data.Enum;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Payroll.Repository
{
    public class BaseSqliteRepository<TEntity> where TEntity : BaseEntity
    {
        private List<object> _prms;

        /// <summary>
        ///
        /// </summary>
        public IDatabase Connection => SqlitePoco.DbFactory.GetDatabase();

        /// <summary>
        ///
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public TEntity Update(TEntity obj)
        {
            try
            {
                using (var db = Connection)
                {
                    obj.UpdatedDate = Convert.ToString(DateTime.UtcNow, CultureInfo.InvariantCulture);
                    db.Update(obj);
                }

                return obj;
            }
            catch (DbUpdateException e)
            {
                Console.WriteLine(e);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return null;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public TEntity Insert(TEntity obj)
        {
            try
            {
                using (var db = Connection)
                {
                    obj.CreatedDate = Convert.ToString(DateTime.UtcNow, CultureInfo.InvariantCulture);
                    obj.UpdatedDate = Convert.ToString(DateTime.UtcNow, CultureInfo.InvariantCulture);
                    obj.RecordStatus = 1;
                    db.Insert(obj);
                }

                return obj;
            }
            catch (DbException e)
            {
                Console.WriteLine(e);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return null;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public int Delete(TEntity obj)
        {
            try
            {
                int result;
                using (var db = Connection)
                {
                    obj.UpdatedDate = Convert.ToString(DateTime.UtcNow, CultureInfo.InvariantCulture);
                    obj.RecordStatus = (int)RecordStatus.Deleted;
                    result = db.Update(obj);
                }

                return result;
            }
            catch (DbException e)
            {
                Console.WriteLine(e);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return 0;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public int Execute(string sql)
        {
            try
            {
                int result;
                using (var db = Connection)
                {
                    result = db.Execute(sql, _prms.ToArray());
                    _prms = null;
                }

                return result;
            }
            catch (DbException e)
            {
                Console.WriteLine(e);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return 0;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public int DeleteFinal(TEntity obj)
        {
            try
            {
                int result;
                using (var db = Connection)
                {
                    result = db.Delete(obj);
                }

                return result;
            }
            catch (DbException e)
            {
                Console.WriteLine(e);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return 0;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public TEntity Get(int id)
        {
            try
            {
                using (var db = Connection)
                {
                    return db.SingleById<TEntity>(id);
                }
            }
            catch (DbException e)
            {
                Console.Write(e);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return null;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        public void AddParam(string key, object obj)
        {
            if (_prms == null) _prms = new List<object>();

            _prms.Add(obj);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public List<TEntity> GetList(string sql)
        {
            using (var db = Connection)
            {
                try
                {
                    if (_prms == null) return db.Fetch<TEntity>(sql);
                    var t = db.Fetch<TEntity>(sql, _prms.ToArray());
                    _prms = null;
                    return t;
                }
                catch (DbException e)
                {
                    Console.WriteLine(e);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public int ExecuteCount(string sql)
        {
            using (var db = Connection)
            {
                try
                {
                    if (_prms == null) return db.Query<int>(sql).FirstOrDefault();
                    var data = db.Fetch<int>(sql, _prms.ToArray());
                    _prms = null;
                    return data.FirstOrDefault();
                }
                catch (DbException e)
                {
                    Console.WriteLine(e);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                return 0;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sort"></param>
        /// <returns></returns>
        public string ApplySort(string sort)
        {
            if (string.IsNullOrEmpty(sort)) return "";
            var lst = new List<string>();
            var lstSort = sort.Split(',');
            foreach (var sortOption in lstSort.Reverse())
                if (sortOption.StartsWith("-"))
                    lst.Add(sortOption.Remove(0, 1) + " desc");
                else
                    lst.Add(sortOption);
            return " Order by " + string.Join(",", lst);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public Page<TEntity> SearchViewSql(string sql, int page, int pagesize)
        {
            using (var db = Connection)
            {
                try
                {
                    if (_prms == null) return db.Page<TEntity>(page, pagesize, sql);
                    var t = db.Page<TEntity>(page, pagesize, sql, _prms.ToArray());
                    _prms = null;
                    return t;
                }
                catch (DbException e)
                {
                    Console.WriteLine(e);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public List<TEntity> SearchAllViewSql(string sql, int page, int pagesize)
        {
            using (var db = Connection)
            {
                try
                {
                    if (_prms == null) return db.Fetch<TEntity>(sql);
                    var t = db.Fetch<TEntity>(sql, _prms.ToArray());
                    _prms = null;
                    return t;
                }
                catch (DbException e)
                {
                    Console.WriteLine(e);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                return null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="field"></param>
        /// <param name="txt"></param>
        /// <returns></returns>
        public string LikeFilter(string field, string txt)
        {
            return " lower(" + field + ") like '%" + txt.ToLower() + "%'";
        }
    }
}
