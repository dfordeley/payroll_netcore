﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Payroll.Data.Data.Contract;
using Payroll.Data.Enum;
using EntityState = Microsoft.EntityFrameworkCore.EntityState;

namespace Payroll.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
    {
        private readonly IUnitOfWork _unitOfWork;

        public Repository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public virtual async Task<List<TEntity>> GetAllAsync(
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null, int? skip = null, int? take = null,
            bool includeDeleted = false)
        {
            return await GetQueryable(null, orderBy, includeProperties, skip, take, includeDeleted).ToListAsync();
        }

        public virtual async Task<List<TEntity>> GetAllAsync(bool includeDeleted = false)
        {
            return await GetQueryable(includeDeleted).ToListAsync();
        }

        public virtual async Task<List<TEntity>> GetAsync(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null, int? skip = null, int? take = null, bool includeDeleted = false)
        {
            return await GetQueryable(filter, orderBy, includeProperties, skip, take, includeDeleted).ToListAsync();
        }

        public virtual async Task<TEntity> GetOneAsync(
            Expression<Func<TEntity, bool>> filter = null,
            string includeProperties = null)
        {
            return await GetQueryable(filter, includeProperties).FirstOrDefaultAsync();
        }

        public virtual async Task<TEntity> GetFirstAsync(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null)
        {
            return await GetQueryable(filter, orderBy, includeProperties).AsNoTracking().FirstOrDefaultAsync();
        }


        public virtual Task<TEntity> GetByIdAsync(int id, bool noTracking = false)
        {
            return noTracking ? _unitOfWork.Context.Set<TEntity>().AsNoTracking().FirstOrDefaultAsync(x => x.Id == id) : _unitOfWork.Context.Set<TEntity>().FirstAsync(x => x.Id == id);
        }

        public virtual TEntity GetById(int id, bool noTracking = false)
        {
            if (noTracking) return _unitOfWork.Context.Set<TEntity>().AsNoTracking().FirstOrDefault(x => x.Id == id);

            return _unitOfWork.Context.Set<TEntity>().Find(id);
        }

        public virtual Task<int> GetCountAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            return GetQueryable(filter, null, false).CountAsync();
        }

        public virtual async Task<bool> GetExistsAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            return await _unitOfWork.Context.Set<TEntity>().AnyAsync(filter);
        }

        public virtual void Create(TEntity entity, string createdBy = null)
        {
            entity.CreatedDate = DateTime.UtcNow;
            entity.ModifiedDate = DateTime.UtcNow;
            entity.Status = RecordStatus.Active;
            _unitOfWork.Context.Set<TEntity>().Add(entity);
            Save();
        }

        public virtual void Update(TEntity entity, string modifiedBy = null)
        {
            entity.ModifiedDate = DateTime.UtcNow;
            _unitOfWork.Context.Set<TEntity>().Attach(entity);
            _unitOfWork.Context.Entry(entity).State = (EntityState) System.Data.Entity.EntityState.Modified;
        }

        public virtual void Delete(int id, string deletedBy = null)
        {
            var entity = _unitOfWork.Context.Set<TEntity>().Find(id);
            if (entity != null) Delete(entity, deletedBy);
        }

        public virtual bool CheckDelete(int id, string deletedBy = null)
        {
            var entity = _unitOfWork.Context.Set<TEntity>().Find(id);
            if (entity != null)
            {
                Delete(entity, deletedBy);
                return true;
            }

            return false;
        }

        public virtual void Delete(TEntity entity, string deletedBy = null)
        {
            var dbSet = _unitOfWork.Context.Set<TEntity>();
            entity.Status = RecordStatus.Deleted;

            if (_unitOfWork.Context.Entry(entity).State == (EntityState) System.Data.Entity.EntityState.Modified)
                dbSet.Attach(entity);
            _unitOfWork.Context.Entry(entity).State = (EntityState) System.Data.Entity.EntityState.Modified;
        }

        public virtual async Task SaveAsync()
        {
            await _unitOfWork.CommitAsync();
        }

        public virtual void Save()
        {
            _unitOfWork.Commit();
        }


        public virtual async Task<TEntity> GetSingleAsync(
            Expression<Func<TEntity, bool>> filter = null,
            string includeProperties = null)
        {
            return await GetQueryable(filter, includeProperties).SingleOrDefaultAsync();
        }

        public virtual async Task CreateMany(List<TEntity> entity)
        {
            await _unitOfWork.Context.Set<TEntity>().AddRangeAsync(entity);
            Save();
        }

        #region Base Query

        protected virtual IQueryable<TEntity> GetQueryable(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null, int? skip = 1, int? take = 10,
            bool includeDeleted = false, bool asNoTracking = false)
        {
            orderBy ??= x => x.OrderBy(o => o.CreatedDate);

            includeProperties ??= string.Empty;

            IQueryable<TEntity> query = _unitOfWork.Context.Set<TEntity>();

            if (!includeDeleted) query = query.Where(x => x.Status != RecordStatus.Deleted);

            if (filter != null) query = query.Where(filter);

            query = includeProperties.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries).Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            query = orderBy(query);

            if (skip.HasValue)
                if (take != null)
                    query = query.Skip((skip.Value - 1) * take.Value).Take(take.Value);

            if (asNoTracking) query = query.AsNoTracking();

            return query;
        }

        public virtual IQueryable<TEntity> GetQueryable(bool includeDeleted = false)
        {
            IQueryable<TEntity> query = _unitOfWork.Context.Set<TEntity>();
            if (!includeDeleted) query = query.AsNoTracking().Where(x => x.Status != RecordStatus.Deleted);
            return query;
        }


        protected virtual IQueryable<TEntity> GetQueryable(
            Expression<Func<TEntity, bool>> filter = null, string includeProperties = null,
            bool includeDeleted = false)
        {
            IQueryable<TEntity> query = _unitOfWork.Context.Set<TEntity>();

            includeProperties ??= string.Empty;

            if (!includeDeleted) query = query.Where(x => x.Status != RecordStatus.Deleted);

            query = includeProperties.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries)
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            return query.AsNoTracking();
        }

        #endregion
    }
}