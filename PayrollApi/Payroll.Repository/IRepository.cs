﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Payroll.Data.Data.Contract;

namespace Payroll.Repository
{
    public interface IRepository<TEntity> where TEntity : IEntity
    {
        Task<List<TEntity>> GetAllAsync(
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null, int? skip = null, int? take = null, bool includeDeleted = false);

        Task<List<TEntity>> GetAllAsync(bool includeDeleted = false);

        Task<List<TEntity>> GetAsync(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null, int? skip = null, int? take = null, bool includeDeleted = false);

        Task<TEntity> GetOneAsync(
            Expression<Func<TEntity, bool>> filter = null,
            string includeProperties = null);

        Task<TEntity> GetFirstAsync(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null);


        Task<TEntity> GetByIdAsync(int id, bool noTracking = false);

        TEntity GetById(int id, bool noTracking = false);

        Task<int> GetCountAsync(Expression<Func<TEntity, bool>> filter = null);

        Task<bool> GetExistsAsync(Expression<Func<TEntity, bool>> filter = null);

        void Create(TEntity entity, string createdBy = null);

        Task CreateMany(List<TEntity> entity);

        void Update(TEntity entity, string modifiedBy = null);

        void Delete(int id, string deletedBy = null);

        bool CheckDelete(int id, string deletedBy = null);

        void Delete(TEntity entity, string deletedBy = null);

        Task SaveAsync();
        void Save();
        IQueryable<TEntity> GetQueryable(bool includeDeleted = false);
    }
}