﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Payroll.Data.Model;
using Payroll.Services.Contract;

namespace Payroll.Api.Controllers
{
    [Route("api/payroll")]
    [ApiController]
    public class PayrollController : ControllerBase
    {
        private readonly ITimeSheetService _timeSheetService;
        public PayrollController(ITimeSheetService timeSheetService)
        {
            _timeSheetService = timeSheetService;
        }

        [Route("Upload")]
        [HttpPost]
        public async Task<IActionResult> Upload(List<TimeSheetModel> timeSheet)
        {
            var (result, message) = await Task.Run(() => _timeSheetService.UploadTimeSheet(timeSheet));

            if (result)
                return Ok("Upload Successful");

            return BadRequest(message);
        }

        [Route("GenerateReport")]
        [HttpPost]
        public async Task<IActionResult> GenerateReport()
        {
            var result = await _timeSheetService.GenerateReport();
            return Ok(result);
        }
    }
}