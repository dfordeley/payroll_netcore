﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Payroll.Data.Entity;
using Payroll.Data.Model;
using Payroll.Tests.TestData;

namespace Payroll.Tests.ServicesTest
{
    public class TimeSheetServiceTest
    {
        private readonly PayrollMock _mocks;
        public TimeSheetServiceTest()
        {
            var mockFactory = new  MockRepository(MockBehavior.Strict){DefaultValue = DefaultValue.Mock};
            _mocks = new PayrollMock(mockFactory);
            _mocks.Setup();
        }

        public void Dispose()
        {
            _mocks.CleanUp();
        }

            
        [Test]
        public void GenerateReport()
        {
            _mocks.Create();

            var ts = _mocks.TimeSheetServiceMock.Object;
            ts.GenerateReport();

            _mocks.MockFactory.Verify();

            var data = new List<TimeSheet> {TestTimeSheets.TimeSheetEntity}.AsQueryable();
            _mocks.RepositoryMock.Setup(x=>x.GetQueryable(true)).Returns(data);

            var call = _mocks.TimeSheetService.GenerateReport();
            var result = call.Result;
            var expected = TestTimeSheets.ReportData;

            _mocks.TimeSheetServiceMock.Verify(x => x.GenerateReport(),Times.Once);

            Assert.NotNull(expected);
            Assert.IsNotEmpty(expected.PayrollReport.EmployeeReports);
            Assert.IsInstanceOf<ReportData>(expected);
            Assert.AreEqual(expected.PayrollReport.EmployeeReports[0].EmployeeId, result.PayrollReport.EmployeeReports[0].EmployeeId);
            Assert.AreEqual(expected.PayrollReport.EmployeeReports[0].AmountPaid, result.PayrollReport.EmployeeReports[0].AmountPaid);
            Assert.AreEqual(expected.PayrollReport.EmployeeReports[0].PayPeriod.StartDate, result.PayrollReport.EmployeeReports[0].PayPeriod.StartDate);
            Assert.AreEqual(expected.PayrollReport.EmployeeReports[0].PayPeriod.EndDate, result.PayrollReport.EmployeeReports[0].PayPeriod.EndDate);
        }

        [Test]
        public void UploadTimeSheet()
        {
            _mocks.Create();

            _mocks.MockFactory.Verify();

            var data = TestTimeSheets.TimeSheetModel;
            data.ReportId = 10;
            var requestData = new List<TimeSheetModel> { TestTimeSheets.TimeSheetModel };

            _mocks.RepositoryMock.Setup(x => x.CreateMany(It.IsAny<List<TimeSheet>>())).Returns(Task.FromResult(0));
            
            var result = _mocks.TimeSheetService.UploadTimeSheet(requestData);

            Assert.AreEqual(result.Item1, true);

        }
    }

}
