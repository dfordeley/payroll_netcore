﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NUnit.Framework;
using Payroll.Data.Entity;
using Payroll.Data.Model;
using Payroll.Repository;
using Payroll.Services.Contract;
using Payroll.Services.Implementation;
using Payroll.Tests.TestData;

namespace Payroll.Tests
{
    public class PayrollMock
    {
        public MockRepository MockFactory { get; }
        private Exception _unhandledException;

        public Mock<IRepository<TimeSheet>>  RepositoryMock { get; set; }
        public Mock<ITimeSheetService> TimeSheetServiceMock { get; set; }

        public ServiceCollection Service { get; }
        public TimeSheetService TimeSheetService { get; set; }

        public PayrollMock(MockRepository mockFactory)
        {
            MockFactory = mockFactory;
            Service = new ServiceCollection();
            RepositoryMock = MockFactory.Create<IRepository<TimeSheet>>(MockBehavior.Loose);
            TimeSheetServiceMock = MockFactory.Create<ITimeSheetService>(MockBehavior.Loose);
            Service.AddScoped<IRepository<TimeSheet>>();
        }
        public void Setup()
        {
            _unhandledException = null;
            var data = new List<TimeSheet> { TestTimeSheets.TimeSheetEntity }.AsQueryable();
            RepositoryMock.Setup(x => x.GetQueryable(It.IsAny<bool>())).Returns(data);
            RepositoryMock.Setup(x => x.CreateMany(It.IsAny<List<TimeSheet>>())).Returns(Task.FromResult(It.IsAny<int>()));
            RepositoryMock.Setup(x => x.CreateMany(It.IsAny<List<TimeSheet>>())).Returns(Task.FromResult(0));
            TimeSheetServiceMock.Setup(x => x.GenerateReport()).Returns(Task.FromResult(It.IsAny<ReportData>()));
            TimeSheetServiceMock.Setup(x => x.UploadTimeSheet(It.IsAny<List<TimeSheetModel>>())).Returns(It.IsAny<Tuple<bool,string>>());
        }

        public void CleanUp()
        {
            MockFactory.Verify();
            if(_unhandledException != null)
                Assert.True(false,$"Unhandled exception occured: {_unhandledException}");
        }

        public void Create()
        {
            TimeSheetService = new TimeSheetService(RepositoryMock.Object);
        }
    }
}
