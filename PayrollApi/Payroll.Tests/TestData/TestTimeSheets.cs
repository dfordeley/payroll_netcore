﻿using Payroll.Data.Entity;
using System;
using System.Collections.Generic;
using Payroll.Data.Enum;
using Payroll.Data.Model;

namespace Payroll.Tests.TestData
{
    public static class TestTimeSheets
    {
        public static TimeSheet TimeSheetEntity { get; } =
            new TimeSheet
            {
                CreatedDate = DateTime.UtcNow,
                Date = new DateTime(2016, 11, 12),
                EmployeeId = "1",
                HoursWorked = 11,
                Id = 1,
                JobGroup = JobGroup.A,
                ModifiedDate = null,
                ReportId = 1,
                Status = RecordStatus.Active
            };
        public static List<TimeSheet> TimeSheetEntityList { get; } =
            new List<TimeSheet>
            {
                new TimeSheet
                {
                    CreatedDate = DateTime.UtcNow,
                    Date = new DateTime(2016, 11, 12),
                    EmployeeId = "1",
                    HoursWorked = 11,
                    Id = 1,
                    JobGroup = JobGroup.A,
                    ModifiedDate = null,
                    ReportId = 1,
                    Status = RecordStatus.Active
                },
                new TimeSheet
                {
                    CreatedDate = DateTime.UtcNow,
                    Date = new DateTime(2016, 11, 16),
                    EmployeeId = "1",
                    HoursWorked = 11,
                    Id = 2,
                    JobGroup = JobGroup.A,
                    ModifiedDate = null,
                    ReportId = 1,
                    Status = RecordStatus.Active
                },
                new TimeSheet
                {
                    CreatedDate = DateTime.UtcNow,
                    Date = new DateTime(2016, 11, 14),
                    EmployeeId = "1",
                    HoursWorked = 5,
                    Id = 3,
                    JobGroup = JobGroup.A,
                    ModifiedDate = null,
                    ReportId = 1,
                    Status = RecordStatus.Active
                },
                new TimeSheet
                {
                    CreatedDate = DateTime.UtcNow,
                    Date = new DateTime(2016, 11, 12),
                    EmployeeId = "2",
                    HoursWorked = 11,
                    Id = 4,
                    JobGroup = JobGroup.B,
                    ModifiedDate = null,
                    ReportId = 1,
                    Status = RecordStatus.Active
                },

            };

        public static TimeSheetModel TimeSheetModel { get; } =
            new TimeSheetModel
            {
                Date = "12/11/2016",
                EmployeeId = "1",
                HoursWorked = 11,
                JobGroup = "A",
                ReportId = 1,
            };
        public static List<TimeSheetModel> TimeSheetModelList { get; } =
            new List<TimeSheetModel>
            {
                new TimeSheetModel()
                {
                    Date = "12/11/2006",
                    EmployeeId = "1",
                    HoursWorked = 11,
                    JobGroup = "A",
                    ReportId = 1
                },
                new TimeSheetModel
                {
                    Date = "16/11/2006",
                    EmployeeId = "1",
                    HoursWorked = 11,
                    JobGroup = "A",
                    ReportId = 1
                },
                new TimeSheetModel
                {
                    Date = "14/11/2006",
                    EmployeeId = "1",
                    HoursWorked = 5,
                    JobGroup = "A",
                    ReportId = 1,
                },
                new TimeSheetModel
                {
                    Date = "12/11/2006",
                    EmployeeId = "2",
                    HoursWorked = 11,
                    JobGroup = "B",
                    ReportId = 1,
                },

            };

        public static ReportData ReportData { get; } =
            new ReportData
            {
                PayrollReport = new PayrollReport
                {
                    EmployeeReports = new List<EmployeeReport>
                    {
                        new EmployeeReport
                        {
                            AmountPaid = "$220.00",
                            EmployeeId = 1,
                            PayPeriod = new PayPeriod
                            {
                                StartDate = "2016-11-01",
                                EndDate = "2016-11-15",

                            }
                        }
                    }
                }
            };

    }
}
