﻿using System.Collections.Generic;
using NUnit.Framework;
using Payroll.Data.Entity;
using Payroll.Data.Factory;
using Payroll.Data.Model;
using Payroll.Tests.TestData;

namespace Payroll.Tests.FactoryTest
{
    public class TimeSheetFactoryTest
    {
        
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestEntityToModelConversion()
        {
            var entity = TestTimeSheets.TimeSheetEntity;
            var expected = TestTimeSheets.TimeSheetModel;
            var convertedData = TimeSheetFactory.ConvertToModel(entity);

            Assert.NotNull(convertedData);
            Assert.IsInstanceOf<TimeSheetModel>(convertedData);
            Assert.AreSame(expected.EmployeeId, convertedData.EmployeeId);
            Assert.AreEqual(expected.HoursWorked, convertedData.HoursWorked);
            Assert.AreEqual(expected.JobGroup, convertedData.JobGroup);
            Assert.AreEqual(expected.ReportId, convertedData.ReportId);
        }

        [Test]
        public void TestModelToEntityConversion()
        {
            var model = TestTimeSheets.TimeSheetModel;
            var expected = TestTimeSheets.TimeSheetEntity;
            var convertedData = TimeSheetFactory.ConvertToEntity(model);

            Assert.NotNull(convertedData);
            Assert.IsInstanceOf<TimeSheet>(convertedData);
            Assert.AreSame(expected.EmployeeId, convertedData.EmployeeId);
            Assert.AreEqual(expected.HoursWorked, convertedData.HoursWorked);
            Assert.AreEqual(expected.JobGroup, convertedData.JobGroup);
            Assert.AreEqual(expected.ReportId, convertedData.ReportId);
        }

        [Test]
        public void TestMultipleModelToEntityConversion()
        {
            var model = TestTimeSheets.TimeSheetModelList;
            var expected = TestTimeSheets.TimeSheetEntityList;
            var convertedData = TimeSheetFactory.ConvertManyToEntity(model);

            Assert.NotNull(convertedData);
            Assert.IsNotEmpty(convertedData);
            Assert.IsInstanceOf<List<TimeSheet>>(convertedData);
            Assert.AreSame(expected[0].EmployeeId, convertedData[0].EmployeeId);
            Assert.AreEqual(expected[1].HoursWorked, convertedData[1].HoursWorked);
            Assert.AreEqual(expected[2].JobGroup, convertedData[2].JobGroup);
            Assert.AreEqual(expected[0].ReportId, convertedData[0].ReportId);
        }
    }
}
