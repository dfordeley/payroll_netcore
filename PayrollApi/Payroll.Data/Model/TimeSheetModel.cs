﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Payroll.Data.Model
{
    public class TimeSheetModel
    {
        [JsonPropertyName("employeeId")]
        [Required]
        public string EmployeeId { get; set; }

        [JsonPropertyName("hoursWorked")]
        [Required]
        public double HoursWorked { get; set; }

        /// <summary>
        /// Format d/MM/yyyy
        /// </summary>
        [JsonPropertyName("date")]
        [RegularExpression("^\\d{1,2}\\/\\d{1,2}\\/\\d{4}$", ErrorMessage = "Date can only be in d/mm/yyyy format")]
        [Required]
        public string Date { get; set; }

        [JsonPropertyName("jobGroup")]
        [Required]
        [StringLength(1, MinimumLength = 1, ErrorMessage = "Job Group Must be supplied")]
        [RegularExpression("A|B", ErrorMessage = "Job Group must be either 'A' or 'B' only.")]
        public string JobGroup { get; set; }

        [JsonPropertyName("reportId")]
        [Required]
        public int ReportId { get; set; }
    }
}
