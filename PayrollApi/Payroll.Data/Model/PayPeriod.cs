﻿using System.Text.Json.Serialization;

namespace Payroll.Data.Model
{
    
    public class PayPeriod
    {
        [JsonPropertyName("startDate")]
        public string StartDate { get; set; }

        [JsonPropertyName("endDate")]
        public string EndDate { get; set; }

    }

    

}
