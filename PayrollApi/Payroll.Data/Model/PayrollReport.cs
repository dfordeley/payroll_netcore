﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Payroll.Data.Model
{
    public class PayrollReport
    {
        [JsonPropertyName("employeeReports")]
        public List<EmployeeReport> EmployeeReports { get; set; }

    }
}
