﻿using System.Text.Json.Serialization;

namespace Payroll.Data.Model
{
    public class EmployeeReport
    {
        [JsonPropertyName("employeeId")]
        public int EmployeeId { get; set; }

        [JsonPropertyName("payPeriod")]
        public PayPeriod PayPeriod { get; set; }

        [JsonPropertyName("amountPaid")]
        public string AmountPaid { get; set; }

    }
}
