﻿using System.Text.Json.Serialization;

namespace Payroll.Data.Model
{
    public class ReportData
    {
        [JsonPropertyName("payrollReport")]
        public PayrollReport PayrollReport { get; set; }

    }
}
