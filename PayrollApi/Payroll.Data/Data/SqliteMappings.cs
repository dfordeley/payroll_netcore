﻿using NPoco.FluentMappings;
using Payroll.Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Payroll.Data.Data
{
    public class SqliteMappings : Mappings
    {
        /// <summary>
        /// 
        /// </summary>
        public SqliteMappings()
        {
            For<TimeSheet>()
                .TableName("TimeSheet");
        }
    }
}
