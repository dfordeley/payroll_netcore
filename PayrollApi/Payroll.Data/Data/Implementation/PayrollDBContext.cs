using Microsoft.EntityFrameworkCore;
using Payroll.Data.Entity;

namespace Payroll.Data.Data.Implementation
{
    public class PayrollDbContext : DbContext
    {
        public PayrollDbContext(DbContextOptions<PayrollDbContext> options) : base(options) { }
        public virtual DbSet<TimeSheet> TimeSheets { get; set; }

    }
}
