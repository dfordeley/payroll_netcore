using System;
using System.Threading.Tasks;
using Payroll.Data.Data.Implementation;

namespace Payroll.Data.Data.Contract
{
    public interface IUnitOfWork : IDisposable
    {
        PayrollDbContext Context { get; }
        Task CommitAsync();
        void Commit();
    }
}
