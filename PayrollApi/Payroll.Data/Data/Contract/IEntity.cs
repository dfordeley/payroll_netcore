﻿using System;
using Payroll.Data.Enum;

namespace Payroll.Data.Data.Contract
{
    public interface IEntity
    {
        int Id { get; set; }
        DateTime CreatedDate { get; set; }
        DateTime? ModifiedDate { get; set; }
        RecordStatus Status { get; set; }
    }
}

