﻿using Microsoft.Data.Sqlite;
using NPoco;
using NPoco.FluentMappings;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;

namespace Payroll.Data.Data
{
    public class SqlitePoco
    {
        public static DatabaseFactory DbFactory { get; set; }
        public static void Setup()
        {
            var serverPath = Directory.GetCurrentDirectory();
            //HttpContext.Current.Server.MapPath("~");
            //{ConfigurationManager.AppSettings["ConnectionString"]}
            var connectionString = $"Data Source= {serverPath}\\DB\\Payroll.db";
            var fluentConfig = FluentMappingConfiguration.Configure(new SqliteMappings());
            DbFactory = DatabaseFactory.Config(x =>
            {
                x.UsingDatabase(() => new Database(connectionString, DatabaseType.SQLite, SqliteFactory.Instance));
                x.WithFluentConfig(fluentConfig);
            });
        }
    }
    
}
