﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Payroll.Common;
using Payroll.Data.Entity;
using Payroll.Data.Enum;
using Payroll.Data.Model;

namespace Payroll.Data.Factory
{
    public static class TimeSheetFactory
    {
        public static TimeSheet ConvertToEntity(TimeSheetModel model)
        {
            
            return new TimeSheet
            {
                Date = DateTime.ParseExact(model.Date, "d/MM/yyyy", CultureInfo.InvariantCulture),
                EmployeeId = model.EmployeeId,
                HoursWorked = model.HoursWorked,
                JobGroup = Constants.ParseEnum<JobGroup>(model.JobGroup),
                ReportId = model.ReportId
            };
        }

        public static List<TimeSheet> ConvertManyToEntity(List<TimeSheetModel> model)
        {
            return model.Select(ConvertToEntity).ToList();
        }
        public static TimeSheetModel ConvertToModel(TimeSheet entity)
        {
            return new TimeSheetModel
            {
                Date = entity.Date.ToShortDateString(),
                EmployeeId = entity.EmployeeId,
                HoursWorked = entity.HoursWorked,
                JobGroup = entity.JobGroup.ToString(),
                ReportId = entity.ReportId
            };
        }
    }
}
