using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Payroll.Common;
using Payroll.Data.Data.Implementation;

namespace Payroll.Data.Factory
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<PayrollDbContext>
    {
        public PayrollDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PayrollDbContext>();
            builder.UseNpgsql(Constants.ConnectionString);
            return new PayrollDbContext(builder.Options);
        }
    }
}
