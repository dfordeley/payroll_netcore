﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Payroll.Data.Entity
{
    public class BaseEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CreatedDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UpdatedDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int RecordStatus { get; set; }
    }
}
