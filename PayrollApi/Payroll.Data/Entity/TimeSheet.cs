﻿using Payroll.Data.Enum;
using System;
using System.Text.Json.Serialization;

namespace Payroll.Data.Entity
{
     public class TimeSheet: Data.Implementation.Entity
     {
         public string EmployeeId { get; set; }
         public double HoursWorked { get; set; }
         public DateTime Date { get; set; }
         public JobGroup JobGroup { get; set; }
         public int ReportId { get; set; }
     }
}
