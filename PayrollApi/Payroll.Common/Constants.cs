﻿using System;

namespace Payroll.Common
{
    public static class Constants
    {
        public static string ConnectionString => "Host=localhost;Database=payroll;Username=postgres;Password=p@55word";

        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }
}
