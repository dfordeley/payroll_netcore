﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Payroll.Data.Model;

namespace Payroll.Services.Contract
{

    public interface ITimeSheetService
    {
        Task<ReportData> GenerateReport();
        Tuple<bool,string> UploadTimeSheet(List<TimeSheetModel> model);

    }
}
