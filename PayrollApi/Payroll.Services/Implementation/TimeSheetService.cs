﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Payroll.Data.Entity;
using Payroll.Data.Enum;
using Payroll.Data.Factory;
using Payroll.Data.Model;
using Payroll.Repository;

namespace Payroll.Services.Implementation
{
    public class TimeSheetService : Contract.ITimeSheetService
    {
        private readonly IRepository<TimeSheet> _repository;

        public TimeSheetService(IRepository<TimeSheet> repository)
        {
            _repository = repository;
        }
        /// <summary>
        /// Generate Report using LINQ to query and filter data
        /// </summary>
        /// <returns></returns>
        public async Task<ReportData> GenerateReport()
        {
            var employeesReport = new List<EmployeeReport>();

            var data = await Task.Run(() => _repository.GetQueryable().ToList());

            var employeeIds = data.Select(x => x.EmployeeId).Distinct();

           
            foreach (var employee in employeeIds)
            {
                GenerateDataForEmployee(employee, data, employeesReport);
            }

            var report = new PayrollReport{EmployeeReports = employeesReport};
            var reportData = new ReportData {PayrollReport = report};
            return reportData;
        }

        /// <summary>
        /// Upload CSV Data returning a Tuple of status and message
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Tuple<bool,string> UploadTimeSheet(List<TimeSheetModel> model)
        {
            try
            {
                if(ReportExist(model[0].ReportId))
                    return new Tuple<bool, string>(false, "Report already exist!");

                var entity = TimeSheetFactory.ConvertManyToEntity(model);

                _repository.CreateMany(entity);

                return new Tuple<bool, string>(true, string.Empty);
            }
            catch (Exception e)
            {
                //Exception should be logged 
                Console.WriteLine(e);
                return new Tuple<bool, string>(false,"Error occured");
            }

        }

        /// <summary>
        /// Checks if file was previously uploaded
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        private bool ReportExist(int reportId)
        {
            return _repository.GetQueryable().Count(x => x.ReportId == reportId) > 0;
        }

        private static void GenerateDataForEmployee(string employeeId, IEnumerable<TimeSheet> data, ICollection<EmployeeReport> employeesReport)
        {
            var employeeGroupedPerMonth = data.Where(x => x.EmployeeId == employeeId)
                  .GroupBy(x => x.Date.Year,
                      (k,
                          g) => g
                   .GroupBy(i => i.Date.Month)).SelectMany(g => g);

            foreach (var monthData in employeeGroupedPerMonth)
            {
                ComputeDataForEachMonth(monthData, employeeId, employeesReport);
            }
        }

        private static void ComputeDataForEachMonth(IEnumerable<TimeSheet> monthData, string employeeId, ICollection<EmployeeReport> employeesReport)
        {
            var monthTimeSheet = monthData.ToList();

            var monthStartDate = monthTimeSheet[0].Date.Date;
            var firstDayOfMonth = new DateTime(monthStartDate.Year, monthStartDate.Month, 1);

            var firstPeriodData = monthTimeSheet.Where(x => x.Date < firstDayOfMonth.AddDays(16));
            var secondPeriodData = monthTimeSheet.Where(x => x.Date > firstDayOfMonth.AddDays(14));

            var firstPeriodHoursWorked = firstPeriodData.Sum(x => x.HoursWorked);
            var secondPeriodHoursWorked = secondPeriodData.Sum(x => x.HoursWorked);

            if (firstPeriodHoursWorked > 0)
            {
                var employeeReport = new EmployeeReport
                {
                    EmployeeId = Convert.ToInt32(employeeId),

                    PayPeriod = new PayPeriod
                    {
                        StartDate = firstDayOfMonth.ToShortDateString(),
                        EndDate = firstDayOfMonth.AddDays(14).ToShortDateString(),
                    },

                    AmountPaid = firstPeriodData.FirstOrDefault().JobGroup == JobGroup.A
                        ? $"{(firstPeriodHoursWorked * 20):C2}"
                        : $"{(firstPeriodHoursWorked * 30):C2}",

                };
                employeesReport.Add(employeeReport);
            }

            if (secondPeriodHoursWorked > 0)
            {
                var employeeReport = new EmployeeReport
                {
                    EmployeeId = Convert.ToInt32(employeeId),

                    PayPeriod = new PayPeriod
                    {
                        StartDate = firstDayOfMonth.AddDays(15).ToShortDateString(),
                        EndDate = firstDayOfMonth.AddMonths(1).AddDays(-1).ToShortDateString(),
                    },

                    AmountPaid = secondPeriodData.FirstOrDefault().JobGroup == JobGroup.A
                        ? $"{(secondPeriodHoursWorked * 20):C2}"
                        : $"{(secondPeriodHoursWorked * 30):C2}",

                };
                employeesReport.Add(employeeReport);
            }
        }

        
    }
}
